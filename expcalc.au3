#cs ----------------------------------------------------------------------------
MIT License

Copyright (c) 2018 jp.nospam (gitlab.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

        AutoIt Version:     3.3.14.2
        Author:             jp.nospam
        Version:            1.1
#ce ----------------------------------------------------------------------------

#RequireAdmin
#pragma compile(Out, expcalc.exe)
#pragma compile(ExecLevel, requireAdministrator)
#pragma compile(Compatibility, win7)
#pragma compile(UPX, False)
#pragma compile(ProductVersion, 1.0)
#pragma compile(FileVersion, 1.0.0.0)

; Custom Includes
#include <Timers.au3>
#include <Misc.au3>

; Allow one instance
_Singleton("expcalc")

; Global Constants
Global Const $START_CALC_BUTTON     = "77"
Global Const $FINISH_CALC_BUTTON    = "78"

; Global Variables
global $hGUI            = GUICreate("", 400, 320)
global $Timer           = NULL
global $Timer_Diff      = NULL
global $Current_Exp     = NULL
global $Exp_Location    = NULL

; Function to ask for input and start the timer
Func StartCalc()

    While _IsPressed($START_CALC_BUTTON)
        sleep(50)
    WEND'

    $Exp_Location   = InputBox("Location", "Input your current location.")
    $Current_Exp    = InputBox("Current Experience", "Input your current experience value.")

    $Timer = _Timer_Init()

EndFunc

; Function to ask for input and finish the timer
Func FinishCalc()

    If $Timer == NULL Then Return

    $Timer_Diff = _Timer_Diff($Timer)
    _Timer_KillTimer($hGUI, $Timer)

    While _IsPressed($FINISH_CALC_BUTTON)
        Sleep(50)
    WEnd

    $finish_exp     = InputBox("Current Experience", "Input your current experience value.")
    $rotation_time  = Round($Timer_Diff/1000, 2)
    $total_exp_gain = Round($finish_exp - $Current_Exp, 4)
    $exp_per_second = Round($total_exp_gain/$rotation_time, 4)
    $exp_per_minute = Round($exp_per_second*60, 4)
    $exp_per_hour   = Round($exp_per_minute*60, 4)
    $results_string = "Location: " & $Exp_Location & @CRLF & "Rotation Time: " & $rotation_time & " seconds" & @CRLF & "Total Experience Gain: " & $total_exp_gain & @CRLF & "Experience Per Second: " & $exp_per_second & @CRLF & "Experience Per Minute: " & $exp_per_minute & @CRLF & "Experience Per Hour: " & $exp_per_hour

    $exp_log = FileOpen("./ExpCalcLog.txt", 1)
    FileWrite($exp_log, $results_string & @CRLF & @CRLF)
    FileClose($exp_log)

    MsgBox(0, "Results", $results_string)

    $Timer = NULL

EndFunc

; Idle loop
While 1
    If _IsPressed($START_CALC_BUTTON) And $Timer == Null Then StartCalc()
    If _IsPressed($FINISH_CALC_BUTTON) And $Timer <> NULL Then FinishCalc()

    sleep(50)
WEnd