Collection of Misc projects/snippets/scripts.

marketremind : AutoIt - A program designed for Black Desert Online to take snapshots of items set on your marketplace 
                        reminders buy list, and let you know when they're about to be available to buy.

warframetools : AutoIt - Contains simple macros and timers for features a game called Warframe lacked.