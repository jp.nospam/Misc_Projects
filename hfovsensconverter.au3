#cs ----------------------------------------------------------------------------
MIT License

Copyright (c) 2018 jp.nospam (gitlab.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
#ce ----------------------------------------------------------------------------


#pragma compile(Out, hfovsensconverter.exe)
#pragma compile(Compatibility, win7)
#pragma compile(UPX, False)
#pragma compile(ProductVersion, 1.0)
#pragma compile(FileVersion, 1.0.0.0)


#include <Math.au3>
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>

$Form1          = GUICreate("HFOV sensitivity converter", 269, 225, 192, 220)
$sensInput      = GUICtrlCreateInput("", 128, 16, 121, 21)
$cfovInput      = GUICtrlCreateInput("", 128, 80, 121, 21)
$nfovInput      = GUICtrlCreateInput("", 128, 112, 121, 21)
$Label2         = GUICtrlCreateLabel("Current FOV", 40, 80, 62, 17)
$Label3         = GUICtrlCreateLabel("New FOV", 48, 112, 50, 17)
$calcButton     = GUICtrlCreateButton("Calculate", 144, 144, 75, 25)
$fsensLabel     = GUICtrlCreateLabel("Source Sensitivity: ", 24, 192, 222, 17)
$matchInput     = GUICtrlCreateInput("", 128, 48, 121, 21)
$Label4         = GUICtrlCreateLabel("Match Distance:", 24, 48, 82, 17)
$cmLabel        = GUICtrlCreateLabel("CM/360: ", 69, 175, 174, 17)
$cmRadio        = GUICtrlCreateRadio("CM/360", 64, 16, 57, 17)
GUICtrlSetState(-1, $GUI_CHECKED)
$sourceRadio    = GUICtrlCreateRadio("Source", 0, 16, 57, 17)
GUISetState(@SW_SHOW)

Global $iniFile = "hdfovc.ini"
ReadSettings()

func ReadSettings()

    $temp = iniread($iniFile, "defaults", "currentsens", "")
    guictrlsetdata($sensInput, $temp)
    $temp = iniread($iniFile, "defaults", "matchdistance", "0.3588")
    guictrlsetdata($matchInput, $temp)
    $temp = iniread($iniFile, "defaults", "currentfov", "")
    guictrlsetdata($cfovInput, $temp)
    $temp = iniread($iniFile, "defaults", "newfov", "")
    guictrlsetdata($nfovInput, $temp)
    $is360 = iniread($iniFile, "defaults", "is360", "0")

    If $is360 == 0 Then
        GUICtrlSetState($sourceRadio, $GUI_CHECKED)
    Else

    Endif

endfunc

Func SourceSensTo360($newSens = 1, $newDPI = 400)

    $base360    = 103.9091
    $baseDPI    = 400
    $baseSens   = 1

    $sensRatio  = $baseSens / $newSens
    $dpiRatio   = $baseDPI / $newDPI
    $new360     = $base360 * $sensRatio * $dpiRatio

    Return $new360

EndFunc

func Source360ToSens($new360, $newDPI = 400)

    $base360    = 103.9091
    $baseDPI    = 400

    $dpiRatio   = $baseDPI / $newDPI
    $newSens    = ($base360 * $dpiRatio) / $new360

    return $newSens

EndFunc

Func CalcFOV()

    $dpi        = iniread($iniFile, "defaults", "dpi", "400")
    $radioValue = GUICtrlRead($cmRadio)
    $sensValue  = GUICtrlRead($sensInput)

    If $radioValue == 1 Then
        $sensValue = Source360ToSens($sensValue, $dpi)
    EndIf

    $cfovValue  = GUICtrlRead($cfovInput)
    $nfovValue  = GUICtrlRead($nfovInput)
    $matchValue = GUICtrlRead($matchInput)

    $cVal       = _Radian($cfovValue/2)
    $nVal       = _Radian($nfovValue/2)
    $multVal    = atan($matchValue*tan($nVal)) / atan($matchValue*tan($cVal))
    $multVal    = round($multVal, 4)
    $finalSens  = round($sensValue * $multVal , 4)
    $Value360   = SourceSensTo360($finalSens, $dpi)
    $Value360   = Round($Value360, 4)

    GUICtrlSetData($cmLabel, "CM/360: " & $Value360)
    GUICtrlSetData($fsensLabel, "Source Sensitivity: " & $finalSens)

EndFunc

While 1
    $nMsg = GUIGetMsg()
    Switch $nMsg
    Case $calcButton
        CalcFOV()
    Case $GUI_EVENT_CLOSE
        Exit
    EndSwitch
WEnd