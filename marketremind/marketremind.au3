#cs ----------------------------------------------------------------------------
MIT License

Copyright (c) 2018 jp.nospam (gitlab.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


   AutoIt Version: 3.3.14.2
   Author:         jp.nospam
   Version:        1.0

#ce ----------------------------------------------------------------------------

#NoTrayIcon
#RequireAdmin
#pragma compile(Out, marketremind.exe)
#pragma compile(ExecLevel, requireAdministrator)
#pragma compile(Compatibility, win7)
#pragma compile(UPX, False)
#pragma compile(ProductVersion, 1.0)
#pragma compile(FileVersion, 1.0.0.0)


AutoItSetOption("MouseCoordMode", 2)

; Custom Includes
#include <ScreenCapture.au3>
#include <Date.au3>
#include <Array.au3>
#include <Timers.au3>
#include <Misc.au3>
#include <File.au3>

; Koda
#include <GUIConstantsEx.au3>
#include <GUIListBox.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#Region ### START Koda GUI section ### Form=
$MainForm       = GUICreate("BDO Market Reminder", 382, 239, 192, 114)
$PictureItem    = GUICtrlCreatePic("", 72, 0, 304, 230, BitOR($GUI_SS_DEFAULT_PIC,$SS_CENTERIMAGE,$WS_BORDER))
$ListMarket     = GUICtrlCreateList("", 0, 0, 65, 227, BitOR($LBS_NOTIFY,$WS_VSCROLL,$WS_BORDER))
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

_Singleton("marketremind")

; Global Constants
Global Const $MEDIA_DIR             = "media/"
Global Const $INI_FILE              = $MEDIA_DIR & "settings.ini"
Global Const $MAX_LIST_TIME         = iniread($INI_FILE, "general", "maximum_list_time", 9)
Global Const $MIN_LIST_TIME         = iniread($INI_FILE, "general", "minimum_list_time", 15)
Global Const $UPDATE_INTERVAL       = iniread($INI_FILE, "general", "update_interval", 30) * 1000
Global Const $BDO_TITLE             = iniread($INI_FILE, "general", "bdo_title", "BLACK DESERT - ")
Global Const $ADD_ITEM_BUTTON       = iniread($INI_FILE, "general", "add_item_key", "77")
Global Const $REMOVE_LAST_BUTTON    = iniread($INI_FILE, "general", "remove_item_key", "78")
;Global Const $GET_MCOORDS_BUTTON   = iniread($INI_FILE, "general", "get_mouse_coords_key", "79")

; Global Variables
Global $Item_List[0][3]
Global $Timer = _Timer_Init()

RestoreItems()

; Function to get mouse coordinates of the BDO client window
func GetMouseCoords()

   While _IsPressed($GET_MCOORDS_BUTTON)
	  Sleep(50)
  WEnd

   $mouse_position = mousegetpos()
   ConsoleWrite($mouse_position[0] & @CRLF)
   ConsoleWrite($mouse_position[1] & @CRLF)

endfunc

; Function to restore the item list if any are possibly still up on the trading post
Func RestoreItems()

    $image_list = _FileListToArray($MEDIA_DIR,"*.jpg")
    If Not IsArray($image_list) Then Return

    For $x = 1 to $image_list[0]
        $file_name              = $MEDIA_DIR & $image_list[$x]
        $file_time              = FileGetTime($file_name)
        $system_time            = _Date_Time_GetLocalTime()
        $current_time_string    = _Date_Time_SystemTimeToDateTimeStr($system_time, 1)
        $file_time_string       = $file_time[0] & "/" & $file_time[1] & "/" & $file_time[2] & " " & $file_time[3] & ":" & $file_time[4] & ":" & $file_time[5]
        $time_diff              = _DateDiff('n', $file_time_string, $current_time_string)

        If $time_diff < $MAX_LIST_TIME Then
             _ArrayAdd($Item_List, $file_time_string, 0)
             $file_id   = StringSplit($image_list[$x], ".")
             $max_index = UBound($Item_List) - 1

             $Item_List[$max_index][1] = 0
             $Item_List[$max_index][2] = $file_id[1]

             _GUICtrlListBox_AddString($ListMarket, $file_time[3] & ":" & $file_time[4])
             _GUICtrlListBox_SetCurSel($ListMarket, $max_index)
        Else
            FileDelete($file_name)
        EndIf
  Next

    UpdatePicture()
    CheckItems()

EndFunc

; Function to add an item to the list and take a snapshot
Func AddItem()

    While _IsPressed($ADD_ITEM_BUTTON)
	  Sleep(50)
  WEnd

    $handle_BDO = WinGetHandle($BDO_TITLE)
    If @error Then Return

    $BDO_size = WinGetClientSize($handle_BDO)

    $shift_X    = $BDO_size[0] - 1920
    $shift_Y    = $BDO_size[1] - 1080
    $coord_X1   = round(1605 + $shift_X)
    $coord_Y1   = round(782 + $shift_Y)
    $coord_X2   = round(1907 + $shift_X)
    $coord_Y2   = round(1002 + $shift_Y)

    $system_time    = _Date_Time_GetLocalTime()
    $current_time   = _Date_Time_SystemTimeToDateTimeStr($system_time, 1)

    _ArrayAdd($Item_List, $current_time, 0)
    $max_index = UBound($Item_List) - 1

    $Item_List[$max_index][1] = 0
    $Item_List[$max_index][2] = @YEAR & @MON & @MDAY & @HOUR & @MIN & @SEC & @MSEC

    _ScreenCapture_SetJPGQuality(100)
    _ScreenCapture_CaptureWnd($MEDIA_DIR & $Item_List[$max_index][2] & ".jpg", $handle_BDO, $coord_X1, $coord_Y1, $coord_X2, $coord_Y2, False)

    _GUICtrlListBox_AddString($ListMarket, @HOUR & ":" & @MIN)
    _GUICtrlListBox_SetCurSel($ListMarket, $max_index)

    UpdatePicture()

EndFunc

; Function to delete an item and it's snapshot from the list
Func DeleteItem($item_index)

   If (UBound($Item_List) - 1) < 0 Then Return

   While _IsPressed($REMOVE_LAST_BUTTON)
	  Sleep(50)
  WEnd

   FileDelete($MEDIA_DIR & $Item_List[$item_index][2] & ".jpg")
   _ArrayDelete($Item_List, $item_index)

   _GUICtrlListBox_DeleteString($ListMarket, $item_index)

   UpdatePicture()

EndFunc

; Function to maintain the list every $UPDATE_INTERVAL seconds
Func CheckItems()

    If UBound($Item_List) < 1 Then Return
    $system_time     = _Date_Time_GetLocalTime()
    $current_time    = _Date_Time_SystemTimeToDateTimeStr($system_time, 1)
    $max_index       = UBound($Item_List) - 1
    $items_removed   = 0

    For $x = 0 To $max_index
        If $x + $items_removed > $max_index Then ExitLoop
        $time_diff = _DateDiff('n', $Item_List[$x][0], $current_time)
        If $time_diff >= $MIN_LIST_TIME and $Item_List[$x][1] == 0 Then
            SoundPlay($MEDIA_DIR & "notice.mp3")
            $Item_List[$x][1] = 1
            $current_text = _GUICtrlListBox_GetText($ListMarket,$x)
            _GUICtrlListBox_ReplaceString($ListMarket, $x, "*" & $current_text & "*")
            _GUICtrlListBox_SetSel($ListMarket)
            _GUICtrlListBox_SetCurSel($ListMarket, $x)
            UpdatePicture($x)
        EndIf

	  If $time_diff >= $MAX_LIST_TIME Then
		 DeleteItem($x)
		 $items_removed += 1
	  EndIf
  Next

EndFunc

; Function to update the picture box to the selected item or $pic_index item
Func UpdatePicture($pic_index = NULL)

   If _GUICtrlListBox_GetCount($ListMarket) < 1 Then
	  GuiCtrlSetImage($PictureItem, "")
	  Return
  EndIf

   $current_selection = $pic_index
   If $pic_index = NULL Then $current_selection = _GUICtrlListBox_GetCurSel($ListMarket)
   If $current_selection = -1 Then Return

   $item_number = $item_list[$current_selection][2]
   GuiCtrlSetImage($PictureItem, $MEDIA_DIR & $item_number & ".jpg")

EndFunc

While 1
    If _IsPressed($ADD_ITEM_BUTTON)      Then AddItem()
    If _IsPressed($REMOVE_LAST_BUTTON)   Then DeleteItem(UBound($Item_List) - 1)
    ;If _IsPressed($GET_MCOORDS_BUTTON)  Then GetMouseCoords()

    If _Timer_Diff($Timer) >= $UPDATE_INTERVAL Then
        _Timer_KillTimer($MainForm, $Timer)
        CheckItems()
        $Timer = _Timer_Init()
    EndIf

    $nMsg = GUIGetMsg()
    Switch $nMsg
    Case $ListMarketUpdatePicture()
    Case $GUI_EVENT_CLOSE
        Exit
    EndSwitch
WEnd