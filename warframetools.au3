#cs ----------------------------------------------------------------------------
MIT License

Copyright (c) 2018 jp.nospam (gitlab.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

    Author:         jp.nospam
    Version:        0.4

    Script Function:
        A warframe helper/macro

    To-Do:
        1. Reorganize functions. Perhaps move to a library.
        2. Add INI support for Mesa Mode to allow flexibility.

    Change Log:
        0.4
             Constant global variable set for Warframe window title for convenience
             purposes.

             Updated attachment box to provide option of canceling.

             Updated keyToggle() to be more efficient. Reorganized main loop to be
             more efficient.

             Added macro for mesa's Peace Maker.

             Replaced all HotKeySet() with _IsPressed().

        0.3
             Fixed fullauto condition to not activate while autofire is on for
             performance purposes.

             Created custom toggling functions for keys to remove system wide API hooks set
             by HotKeySet().

             Will focus on warframe after hiding and showing GUI.

        0.2
             Moved all toggle variables into 1 array.

             Added autofire function.

        0.1
             Updated timers to not show if timer is set to 0.
#ce ----------------------------------------------------------------------------

#NoTrayIcon

; Custom includes
#include <WinAPI.au3>
#include <Misc.au3>

; Koda includes
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>

const $WFTITLE = "WARFRAME"

while not winexists($WFTITLE)
    $decision = msgbox(69, "Unable to attach!", "Unable to attach to warframe.")
    if $decision == 2 then exit
wend

; Set options
AutoItSetOption("WinTitleMatchMode", 3)
AutoItSetOption("WinSearchChildren", 1)
AutoItSetOption("SendCapslockMode", 0)
AutoItSetOption("SendKeyDownDelay", 0)
AutoItSetOption("SendKeyDelay", 0)


; Set constant globals
const $guiScale    = 64
const $sFile       = "wfTool.ini"
const $wfHandle    = wingethandle($WFTITLE)
const $hDLL        = DllOpen("user32.dll")

; Independent global vars
global $timerNum    = 4
global $timerX      = 0
global $timerY      = 0
global $labelXPOS   = 8
global $inputXPOS   = 8
global $statTimer   = 0
global $fSize
global $togs[5]
global $statDuration
global $autoFireKey
global $fullAutoKey
global $mesaVar = 0

; Read settings file
readSettings($sFile)

; Dependent global vars
$sizeX = 67 * $timerNum
if $sizeX < 216 then $sizeX = 216
global $sTimer[$timerNum]
global $sLabel[$timerNum]
global $sInput[$timerNum]
global $tMacro[$timerNum]


; =====START GUI CREATION=====
; Dynamically generate form
$timerForm = GUICreate("Warframe Tools", $sizeX, 137, $timerX, $timerY, $WS_POPUPWINDOW, $WS_EX_LAYERED)
GUISetBkColor(0x000000)
_WinAPI_SetLayeredWindowAttributes($timerForm, 0x000000, 255)
; Create status label
$statLabel = GUICtrlCreateLabel("FullAuto: OFF", 8, 15, 216, 17)
GUICtrlSetFont(-1, 12, 400, 0, "MS Sans Serif")
GUICtrlSetColor(-1, 0xFF0000)
guictrlsetstate(-1, $GUI_HIDE)
; Dynamically generate input and label controls for timers
for $x = 0 to $timerNum - 1
    $sInput[$x] = GUICtrlCreateInput("0", $inputXPOS, 35, 49, 24, BitOR($GUI_SS_DEFAULT_INPUT,$ES_NUMBER))
    GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
    GUICtrlSetColor(-1, 0xFF0000)

    $sLabel[$x] = GUICtrlCreateLabel("0", $labelXPOS, 59, 28, 32)
    GUICtrlSetFont(-1, $fSize, 800, 0, "MS Sans Serif")
    GUICtrlSetColor(-1, 0xFF0000)
    guictrlsetstate(-1, $GUI_HIDE)

    $inputXPOS += $guiScale
    $labelXPOS += $guiScale
next
GUISetState(@SW_SHOW)
; =====FINISH GUI CREATION=====

; Window containment stuff
_WinAPI_SetParent($timerForm, $wfHandle)

#cs
    Function:        readSettings

    Parameters:
       $settings     The settings file to be read

    Return:
       None

    Description:     Reads the settings file
#ce
func readSettings($settings)

    ; Timer stuff
    $timerX    = iniread($settings, "settings", "tx", 0)
    $timerY    = iniread($settings, "settings", "ty", 0)
    $timerNum  = iniread($settings,"settings", "timerNumber", 4)

    if $timerNum < 1 then $timerNum = 1
    if $timerNum > 9 then $timerNum = 9

    $fSize          = iniread($settings, "settings", "timerSize", 10) ; load timer size
    $autoFireKey    = iniread($settings, "settings", "autoFireKey", 0x2E) ;set autofire hotkey
    $fullAutoKey    = iniread($settings, "settings", "fullAutoKey", 0x09) ; set full auto hotkey

endfunc

#cs
    Function:        saveSetting

    Parameters:
       $val           The value of the key

       $key           The key name

       $section       Section of the ini to write to

       $settings     The ini file to be written to

    Return:
       None

    Description:     Writes all settings to the settings file
#ce
func saveSetting($val, $key, $section = "settings", $settings = $sFile)

    iniwrite($settings, $section, $key, $val)

endfunc

#cs
    Function:        quickLoad

    Parameters:
       $settingNum    The preset setting number

    Return:
       None

    Description:     Loads designated preset from ini
#ce
func quickLoad($settingNum)

    resetAllTimers()

    for $x = 0 to $timerNum - 1
        guictrlsetdata($sInput[$x], iniread($sFile, "quick"&$settingNum, "time"&$x+1, 0))
    next

endfunc

#cs
    Function:        quickSave

    Parameters:
       $settingNum    The preset setting number

    Return:
       None

    Description:     Saves desginated preset to ini
#ce
func quickSave($settingNum)

    for $x = 0 to $timerNum - 1
        iniwrite($sFile, "quick"&$settingNum, "time"&($x+1), guictrlread($sInput[$x]))
    next

endfunc

#cs
    Function:        statDisplay

    Parameters:
        $message    The message to be displayed in the status label

        $duration    How long the status message should last in milliseconds
                     Default: 2 Seconds

    Return:
        None

    Description:    Sets the status label to display a message for a period of time
#ce
func statDisplay($message, $duration = 2000)

    $statTimer = timerinit()
    guictrlsetdata($statLabel, $message)
    $statDuration = $duration

endfunc

#cs
    Function:            incFont

    Parameters:
        None

    Return:
        None

    Description:        Increases the font size of the timers
#ce
func incFont()

    if $fSize = 25 then return
    $fSize += 1

    saveSetting($fSize, "timerSize")

    for $x = 0 to $timerNum - 1
          guictrlsetfont($sLabel[$x],$fSize,400)
     next

    statDisplay("Font size: " & $fSize, 2000)

endfunc

#cs
    Function:        decFont

    Parameters:
       None

    Return:
       None

    Description:     Decreases the font size of the timers
#ce
func decFont()

    if $fSize = 0 then return
    $fSize -= 1

    saveSetting($fSize, "timerSize")

    for $x = 0 to $timerNum - 1
        guictrlsetfont($sLabel[$x],$fSize,400)
    next

    statDisplay("Font size: " & $fSize, 2000)

endfunc

#cs
    Function:        toggleClutter

    Parameters:
       None

    Return:
       None

    Description:     Function to hide/show the clutter
#ce
func toggleClutter()

     if $togs[1] Then
          for $x = 0 to $timerNum - 1
              guictrlsetstate($sInput[$x],$GUI_HIDE)
          next
     EndIf

    if not $togs[1] Then
        for $x = 0 to $timerNum - 1
           guictrlsetstate($sInput[$x],$GUI_SHOW)
        next
    endif

endfunc

#cs
    Function:        toggleTimers

    Parameters:
       None

    Return:
       None

    Description:     Function to hide/show the timers
#ce
func toggleTimers()

    if $togs[0] then winsetstate($timerForm, "", @SW_HIDE)

    if not $togs[0] then
        winsetstate($timerForm, "", @SW_SHOW)
        winactivate($wfHandle)
    endif

endfunc

#cs
    Function:        timerMove

    Parameters:
       None

    Return:
       None

    Description:     Function to move the timer window to mouse coordinates
#ce
func timerMove()

    $mpos = mousegetpos()

    winmove($timerForm, "", $mpos[0], $mpos[1])

    saveSetting($mpos[0], "tx")
    saveSetting($mpos[1], "ty")
endfunc

#cs
    Function:        initTimer

    Parameters:
       $timer        A timer handle created by TimerInit()

       $label        Handle of a label

    Return:
       None

    Description:     Inititalizes a timer
#ce
func initTimer(byref $timer, byref $label)

    $timer = timerInit()

    guictrlsetstate($label, $GUI_SHOW)

endfunc

#cs
    Function:        resetAllTimers

    Parameters:
       None

    Return:
       None

    Description:     Resets all the timers
#ce
func resetAllTimers()

    for $x = 0 to $timerNum - 1
        $sTimer[$x] = 1
    next

EndFunc

#cs
    Function:        cleanTime

    Parameters:
       $timer        A timer handle called by TimerInit()

    Return:
       A rounded number with no decimal places (integer?)

    Description:     A function to clean the time returned by TimerDiff()
                     and convert its value into seconds
#ce
func cleanTime(byref $timer)

    $currentTime = timerdiff($timer)/1000
    return round($currentTime,0)

endfunc

#cs
    Function:        timerMaint

    Parameters:
       $timer        A timer handle called by TimerInit()

       $end           The maximum alloted time for the timer

       $label        The label handle to update the timer value

    Return:
        None

    Description:     Cleans time values to seconds, updates timer
                     labels, initializes and maintains 1-9 macro mode.
#ce
func timerMaint(byref $timer, $end, byref $label, $tNum)

    guictrlsetdata($label, $end - cleanTime($timer))

    if cleanTime($timer) > $end Then
        $timer = 0
        guictrlsetdata($label, 0)
        guictrlsetstate($label, $GUI_HIDE)
        if $tMacro[$tNum] then
            send($tNum+1)
            initTimer($timer, $label)
        endif
    endif

EndFunc

#cs
    Function:        keyToggle

    Parameters:
        $key        Key to activate toggle

        $toggle     Variable to that holds toggle value

        $keyalt     Alternate key (I.E shift, control, alt, etc...)

    Return:
        TRUE        If toggle was switched

        FALSE        If toggle was not switched

    Description:    Function to handle variable toggling
#ce
func keyToggle($key, byref $toggle, $keyalt = 0x00)

    if _IsPressed($key, $hDLL) and $keyalt = 0x00 Then
        $toggle = not $toggle
        while _IsPressed($key, $hDLL)
        wend
        return 1
    endif

    if _IsPressed($key, $hDLL) and _IsPressed($keyalt, $hDLL) Then
        $toggle = not $toggle
        while _IsPressed($key, $hDLL) and _IsPressed($keyalt, $hDLL)
        wend
        return 1
    endif

    return 0

endfunc

#cs
    Function:        statStateController

    Return:
        $GUI_SHOW

        $GUI_HIDE

    Description:    Controlls state of status message
#ce
func statStateController()

    if $statTimer <> 0 Then
        if timerdiff($statTimer) >= $statDuration then
             $statTimer = 0
             return $GUI_HIDE
        else
             if guictrlgetstate($statLabel) <> $GUI_SHOW then return $GUI_SHOW
        endif
    endif

    if $statTimer = 0 then return  $GUI_HIDE

endfunc

While 1
    $nMsg = GUIGetMsg()

    if not WinExists($WFTITLE) then EXIT

    if not winactive($WFTITLE) then ContinueLoop

    for $x = 1 to $timerNum

        ; Initialize timer if key is pressed
        if _ispressed(30+$x, $hDLL) and guictrlread($sInput[$x-1]) > 0 then initTimer($sTimer[$x-1], $sLabel[$x-1])

        ; Perform macro toggle
        keyToggle(60+$x, $tMacro[$x-1], 12)
        if not $tMacro[$x-1] then guictrlsetcolor($sLabel[$x-1], 0xff0000)
        if $tMacro[$x-1] then guictrlsetcolor($sLabel[$x-1], 0x00ff00)

    next

    ; Set toggles for hiding showing gui/timers
    if keyToggle(71, $togs[0]) then toggleTimers()
    if keyToggle(72, $togs[1]) then toggleClutter()

    ; FullAuto Toggle
    if keyToggle($fullAutoKey, $togs[2]) then
        if $togs[2] then statDisplay("FullAuto: ON")
        if not $togs[2] then
             statDisplay("FullAuto: OFF")
             mouseup("left")
        endif
    endif

    ; FullAuto Macro
    if _IsPressed(1, $hDLL) and $togs[2] and not $togs[3] then
        mouseup("left")
        mousedown("left")
    endif

    ; AutoFire Toggle
    if keyToggle($autoFireKey, $togs[3]) then
        if $togs[3] then statDisplay("AutoFire: ON")
        if not $togs[3] then statDisplay("AutoFire: OFF")
    endif

    ; AutoFire Macro
    if $togs[3] then mouseclick("left")

    ; MesaMode Toggle
    if keyToggle(76, $togs[4]) then
        if $togs[4] then statDisplay("Mesa Mode: ON")
        if not $togs[4] then
             statDisplay("Mesa Mode: OFF")
             if $mesaVar == 1 then
                 mouseup("left")
                 $mesaVar = 0
             endif
        endif
    endif

    ; Mesa Mode Macro
    if $togs[4] and _IsPressed("34", $hDLL) then
        if $mesaVar == 0 then
             sleep(500)
             mousedown("left")
             $mesaVar = 1
        Else
             sleep(500)
             mouseup("left")
             $mesaVar = 0
        endif
    endif

    ; Show/hide status messages
    guictrlsetstate($statLabel, statStateController())

    ; Misc Hotkeys
    if _IsPressed(70, $hDLL)    then timerMove()
    if _IsPressed("6B", $hDLL)  then incFont()
    if _IsPressed("6D", $hDLL)  then decFont()

    ; Load/save setting if key is pressed
    For $x = 1 to 9
        if _ispressed(11, $hDLL) and _ispressed(60+$x, $hDLL) then quickSave($x)
        if _ispressed(60+$x, $hDLL) and not _ispressed(11, $hDLL) and not _ispressed(12, $hDLL) then quickload($x)
    next

    ; Timer maintenance
    for $x = 0 to $timerNum - 1
        if $sTimer[$x] > 0 then timerMaint($sTimer[$x], guictrlread($sInput[$x]), $sLabel[$x], $x)
    next

    Switch $nMsg
        Case $GUI_EVENT_CLOSE
             DllClose($hDLL)
             exit
    EndSwitch
WEnd
